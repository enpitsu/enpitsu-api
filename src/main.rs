#![feature(proc_macro_hygiene, decl_macro)]

mod task;
mod get_tasks;
mod delete_tasks;
// mod update_tasks;

extern crate chrono;
#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
extern crate enpitsu_lib;

use rocket_contrib::json::{ Json, JsonValue };
use enpitsu_lib::{EnpDB};
use std::path;
use crate::task::Task;

#[get("/")]
fn index() -> &'static str {
    "Enpitsu API is ready!

    GET
        /tasks              Return array of tasks

    POST
        /new                Add new task
            Content-Type    : application/json
            Required fields :
                title       : String,
                startdate   : String (eg: 1920-03-11),
                starttime   : String (eg: 10:24: 11),
                enddate     : String (eg: 1999-10-21),
                endtime     : String (eg: 09:10: 20),
                timezone    : String (eg: -08:00),
                tags        : String (eg: tag 1, tag 2),
                priority    : String (options: No Priority, Important, Urgent, Important and Urgent),
                notes       : String,
                star        : bool,

    DELETE
        /remove/<id>        Delete task with id
        "
}

#[get("/tasks?<limit>&<offset>")]
fn get_tasks(limit: Option<isize>, offset: Option<isize>) -> Json<JsonValue> {
    let db = EnpDB::new(path::PathBuf::from("/home/japorized/.data/enpitsu/tasks.db".to_owned()));
    let limit_value = match limit {
        Some(l) => l,
        None => 50,
    };
    let tasks = match db.get_tasks(Some(limit_value), offset) {
        Ok(t) => t,
        Err(err) => {
            println!("{}", err);

            vec![]
        }
    };
    let prev = match offset {
        Some(o) => {
            if tasks.len() == 0 || o == 0 {
                "".to_owned()
            } else if limit_value > o {
                format!("/tasks?limit={}&offset=0", limit_value)
            } else {
                format!("/tasks?limit={}&offset={}", limit_value, o - limit_value)
            }
        },
        None => "".to_owned()
    };
    let next = match offset {
        Some(o) => {
            if tasks.len() == 0 || limit_value + o >= db.get_total().expect("Could not get total") {
                "".to_owned()
            } else {
                format!("/tasks?limit={}&offset={}", limit_value, limit_value + o)
            }
        },
        None => {
            if tasks.len() == 0 || limit_value >= db.get_total().expect("Could not get total") {
                "".to_owned()
            } else {
                format!("tasks?limit={}&offset={}", limit_value, limit_value)
            }
        }
    };
    
    Json(json!({
        "status" : "Success",
        "prev" : prev,
        "next" : next,
        "tasks" : tasks
    }))
}

#[post("/new", format = "application/json", data = "<task_data>")]
fn new_task(task_data: Json<Task>) -> Json<JsonValue> {
    let db = EnpDB::new(path::PathBuf::from("/home/japorized/.data/enpitsu/tasks.db".to_owned()));
    let mut task = Task::new_task(
        task_data.title.clone(),
        task_data.startdate.clone(),
        task_data.starttime.clone(),
        task_data.enddate.clone(),
        task_data.endtime.clone(),
        task_data.timezone.clone(),
        task_data.tags.clone(),
        task_data.priority.clone(),
        task_data.notes.clone(),
        task_data.star
        );

    match db.add_task(&task) {
        Ok(o) => {
            task.id = o;

            Json(json!({ "status" : "Success", "task" : task }))
        },
        Err(err) => {
            println!("enpitsu-api Error: {}", err);

            Json(json!({ "status" : "Failed to create task", "task" : [] }))
        }
    }
}

fn main() {
    rocket::ignite()
        .mount("/", routes![index, get_tasks, new_task])
        .mount("/tasks", routes![get_tasks::get_task_by_id])
        .mount("/remove", routes![delete_tasks::delete_tasks])
        // .mount("/update", routes![update_tasks::update_task])
        .launch();
}
