FROM rustlang/rust:nightly as builder
WORKDIR /usr/app/enpitsu-api
COPY . .
RUN cargo install --path .

FROM debian:buster-slim
RUN apt-get update && apt-get -y install sqlite3 libsqlite3-dev
COPY --from=builder /usr/local/cargo/bin/enpitsu-api /usr/local/bin/enpitsu-api

EXPOSE 8080

CMD ["/usr/local/bin/enpitsu-api"]
