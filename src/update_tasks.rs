use rocket::response::status;
use rocket_contrib::json::{ Json, JsonValue };
use std::path;
use enpitsu_lib::{EnpDB};
use crate::task::PartialTask;

#[put("/<id>", format = "application/json", data = "<task_data>")]
pub fn update_task(id: isize, task_data: Json<PartialTask>) -> Json<JsonValue> {
    let db = EnpDB::new(path::PathBuf::from("/home/japorized/.data/enpitsu/tasks.db".to_owned()));
    let existing_task = db.get_tasks_by_ids(&vec![id]).expect("Could not get task with the given id");
    let updated_task = Task::new_task(
        match task_data.title {
            Some(t) => t,
            None => existing_task.title,
        },
        task_data.startdate.clone(),
        task_data.starttime.clone(),
        task_data.enddate.clone(),
        task_data.endtime.clone(),
        task_data.timezone.clone(),
        task_data.tags.clone(),
        task_data.priority.clone(),
        task_data.notes.clone(),
        task_data.star
        );
}
