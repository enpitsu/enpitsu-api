use rocket::response::status;
use rocket_contrib::json::{ Json, JsonValue };
use std::path;
use enpitsu_lib::{EnpDB};

#[delete("/<id>")]
pub fn delete_tasks(id: isize) -> status::Accepted<Json<JsonValue>> {
    let db = EnpDB::new(path::PathBuf::from("/home/japorized/.data/enpitsu/tasks.db".to_owned()));
    let msg: String = match db.remove_task(id) {
        Ok(_p) => "Success".to_string(),
        Err(_e) => "Unsuccessful. Please contact administrator".to_string()
    };

    status::Accepted(Some(Json(json!({ "status" : msg }))))
}

