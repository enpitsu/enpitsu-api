# Enpitsu API

A RESTful API written using Rust with [rocket.rs](https://rocket.rs/)

---

## Setup

```
git clone https://gitlab.com/enpitsu/enpitsu-api
cd enpitsu-api
cargo run
```

---

## Notes

* Most of the basic stuff are in, except for PUT requests (so the U in CRUD)
* Not completely RESTful yet; pagination is available only to certain endpoints
  (may need to rethink how to do this more generically?)
