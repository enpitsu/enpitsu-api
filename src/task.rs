use chrono::prelude::*;
use serde::{Serialize, Deserialize};
use enpitsu_lib::models;
use rocket::request::FromForm;
use std::{
    str::FromStr
};

/// Structure for a Task in Enpitsu
#[derive(Serialize, Deserialize, Debug, FromForm)]
pub struct Task {
    /// ID of the Task
    pub id: Option<isize>,
    /// Title of the Task
    pub title: String,
    /// Beginning date of the Task
    pub startdate: String,
    /// Beginning time of the Task
    pub starttime: String,
    /// Ending date of the Task
    pub enddate: String,
    /// Ending time of the Task
    pub endtime: String,
    /// Timezone of where the Task is created
    pub timezone: String,
    /// Tags of the task
    pub tags: String,
    /// Priority of the Task
    pub priority: String,
    /// Notes for the Task
    pub notes: String,
    /// Status of the Task
    pub status: Option<String>,
    /// Is the Task starred?
    pub star: bool,
}

impl Task {
    pub fn new_task(
        title: String,
        startdate: String,
        starttime: String,
        enddate: String,
        endtime: String,
        timezone: String,
        tags: String,
        priority: String,
        notes: String,
        star: bool) -> models::Task
    {
        let raw_startdatetime = format!("{}T{}{}", startdate, starttime, timezone);
        let raw_enddatetime = format!("{}T{}{}", enddate, endtime, timezone);
        let startdatetime = match DateTime::parse_from_rfc3339(&raw_startdatetime) {
            Ok(s) => Some(s),
            Err(err) => {
                println!("enpitsu-api Error: {}", err);

                None
            }
        };
        let enddatetime = match DateTime::parse_from_rfc3339(&raw_enddatetime) {
            Ok(s) => Some(s),
            Err(err) => {
                println!("enpitsu-api Error: {}", err);

                None
            }
        };

        models::Task {
            id: 0, // Dummy value. Will not get input into db
            title,
            startdatetime,
            enddatetime,
            tags: enpitsu_lib::utils::tag_str_to_vec(tags),
            priority: models::Priority::from_str(&priority)
                .expect("Could not parse priority input"),
            notes,
            status: models::Status::Active,
            star,
            creation: Local::now().with_timezone(&FixedOffset::west(8*3600))
        }
    }
}

/// A partial version of Task where all fields are Options
pub struct PartialTask {
    /// Title of the Task
    pub title: Option<String>,
    /// Beginning date of the Task
    pub startdate: Option<String>,
    /// Beginning time of the Task
    pub starttime: Option<String>,
    /// Ending date of the Task
    pub enddate: Option<String>,
    /// Ending time of the Task
    pub endtime: Option<String>,
    /// Timezone of where the Task is created
    pub timezone: Option<String>,
    /// Tags of the task
    pub tags: Option<String>,
    /// Priority of the Task
    pub priority: Option<String>,
    /// Notes for the Task
    pub notes: Option<String>,
    /// Status of the Task
    pub status: Option<String>,
    /// Is the Task starred?
    pub star: Option<bool>,
}
