use rocket_contrib::json::{ Json, JsonValue };
use std::path;
use enpitsu_lib::{EnpDB};

#[get("/<id>")]
pub fn get_task_by_id(id: isize) -> Json<JsonValue> {
    let db = EnpDB::new(path::PathBuf::from("/home/japorized/.data/enpitsu/tasks.db".to_owned()));
    let task = match db.get_tasks_by_ids(&vec![id]) {
        Ok(t) => t,
        Err(err) => {
            println!("{}", err);

            vec![]
        }
    };

    let prev_url = if id > 1 {
        format!("/tasks/{}", id - 1)
    } else {
        "".to_owned()
    };
    let next_url = if id == db.get_total().expect("Could not get total") {
        "".to_owned()
    } else {
        format!("/tasks/{}", id + 1)
    };

    Json(json!({
        "status": "Success",
        "prev": prev_url,
        "next": next_url,
        "tasks": task
    }))
}

